#!/bin/bash

echo "Start elastic service"
/etc/init.d/elasticsearch start

echo "Start kibana service"
/etc/init.d/kibana start

echo "run ssh"
/usr/sbin/sshd -D

echo "done"