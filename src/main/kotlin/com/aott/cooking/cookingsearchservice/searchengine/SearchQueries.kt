package com.aott.cooking.cookingsearchservice.searchengine

import com.aott.cooking.cookingsearchservice.model.recipe.Recipe

abstract class SearchQueries {
    abstract fun simpleSearch(keywords: String): List<Recipe>
}