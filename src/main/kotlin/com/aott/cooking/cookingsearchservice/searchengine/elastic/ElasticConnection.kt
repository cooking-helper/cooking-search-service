package com.aott.cooking.cookingsearchservice.searchengine.elastic

import org.apache.http.HttpHost
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class ElasticConnection @Autowired constructor(
        config: ElasticConfig
) {
    val client: RestHighLevelClient = createClient(config)

    private fun createClient(config: ElasticConfig): RestHighLevelClient {
        try {
            return RestHighLevelClient(
                    RestClient.builder(
                            HttpHost(config.getServer(), Integer.parseInt(config.getPort()), "http")).build())

        } catch (e: Exception) {
            throw Exception(e)
        }
    }
}