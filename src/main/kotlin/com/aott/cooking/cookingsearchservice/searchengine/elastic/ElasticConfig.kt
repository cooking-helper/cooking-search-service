package com.aott.cooking.cookingsearchservice.searchengine.elastic

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource


@Configuration
@PropertySource("\${spring.config.location:}")
@ConfigurationProperties(prefix = "search.engine.elastic")
class ElasticConfig {
    private var server = "localhost"
    private var port = "9200"

    fun setPort(portVal: String) {
        port = portVal
    }

    fun setServer(serverVal: String) {
        server = serverVal
    }

    fun getServer(): String {
        return server
    }

    fun getPort(): String {
        return port
    }
}