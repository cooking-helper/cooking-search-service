package com.aott.cooking.cookingsearchservice.searchengine.elastic

import com.aott.cooking.cookingsearchservice.model.recipe.Recipe
import com.aott.cooking.cookingsearchservice.searchengine.SearchQueries
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository


@Repository
class ElasticSearchQueries @Autowired constructor(
        private val elastic: ElasticConnection
) : SearchQueries() {
    val INDEX = "cooking"
    val TYPE = "recipe"

    override fun simpleSearch(keywords: String): List<Recipe> {
        val recipes: MutableList<Recipe> = mutableListOf()
        val searchRequest = SearchRequest(INDEX)
        searchRequest.types(TYPE)
        val searchSourceBuilder = SearchSourceBuilder()
        searchSourceBuilder.query(QueryBuilders.multiMatchQuery(keywords, "title", "instructions", "ingredients.name"))
        searchSourceBuilder.from(0)
        searchSourceBuilder.size(10)
        searchRequest.source(searchSourceBuilder)

        val searchScrollResponse = elastic.client.search(searchRequest)
        val searchHits = searchScrollResponse.hits.hits

        if (searchHits != null && searchHits.isNotEmpty()) {
            for (hit in searchHits) {
                val genericRecipe = hit.sourceAsMap
                val mapper = ObjectMapper()
                        .enable(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES)
                        .registerModule(KotlinModule())
                val recipe = mapper.convertValue(genericRecipe, Recipe::class.java)
                recipes.add(recipe)
            }
        }
        return recipes
    }
}
