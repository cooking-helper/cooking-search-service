package com.aott.cooking.cookingsearchservice.model.recipe

import java.util.*

data class Recipe(
        val url: String = "",
        val updateDate: Date,
        val title: String = "",
        val totalTime: Int = 0,
        val prepareTime: Int = 0,
        val cookingTime: Int = 0,
        val difficulty: Int = 0,
        val cost: Int = 0,
        val numberOfPiece: Int = 0,
        val ingredients: List<Ingredient> = ArrayList(),
        val instructions: String = ""
)
