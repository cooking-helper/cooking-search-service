package com.aott.cooking.cookingsearchservice.model.recipe

data class Ingredient(val name: String, val quantity: Int, val unit: String)
