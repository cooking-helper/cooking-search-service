package com.aott.cooking.cookingsearchservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CookingSearchServiceApplication

fun main(args: Array<String>) {
    runApplication<CookingSearchServiceApplication>(*args)
}
