package com.aott.cooking.cookingsearchservice.rest.controller

import com.aott.cooking.cookingsearchservice.model.recipe.Recipe
import com.aott.cooking.cookingsearchservice.searchengine.elastic.ElasticSearchQueries
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/search")
class SearchController @Autowired constructor(
        private val searchQueries: ElasticSearchQueries
) {
    @GetMapping
    fun simpleSearch(@RequestParam(value = "keywords") keywords: String): List<Recipe> {
        return searchQueries.simpleSearch(keywords)
    }
}