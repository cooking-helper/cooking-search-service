package com.aott.cooking.search

import com.aott.cooking.cookingsearchservice.CookingSearchServiceApplication
import com.aott.cooking.cookingsearchservice.rest.controller.SearchController
import com.aott.cooking.cookingsearchservice.searchengine.elastic.ElasticConfig
import com.aott.cooking.search.util.ElasticQueryUtil
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForObject
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [CookingSearchServiceApplication::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = ["classpath:test.properties"])
class SimpleSearchTests {

    @LocalServerPort
    private val port: Int = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null

    @Autowired
    private val controller: SearchController? = null

    @Autowired
    private val elasticConfig: ElasticConfig? = null

    private var elasticQueryUtil: ElasticQueryUtil? = null

    @Before
    fun cleanDatabase() {
        if (elasticConfig != null) {
            elasticQueryUtil = ElasticQueryUtil(elasticConfig)
        } else {
            throw ExceptionInInitializerError()
        }
    }

    @Test
    fun contextLoads() {
        Assertions.assertThat(controller).isNotNull();
    }

    @Test
    fun testSimpleSearchTitleMatch() {
        insertTarteCarottesOignons()
        val keywords = "tarte"
        val result: List<Map<String, Any>>? = this.restTemplate?.getForObject("http://localhost:$port/search?keywords=$keywords")
        Assertions.assertThat(result).isNotNull
        Assertions.assertThat(result!!.size).isEqualTo(1)
        val recipe: Map<String, Any> = result.get(0)
        Assertions.assertThat(recipe?.get("title")).isEqualTo("Tarte aux carottes et oignons")
    }

    @Test
    fun testSimpleSearchIngredientMatch() {
        insertTarteCarottesOignons()
        val keywords = "carvi"
        val result: List<Map<String, Any>>? = this.restTemplate?.getForObject("http://localhost:$port/search?keywords=$keywords")
        Assertions.assertThat(result).isNotNull
        Assertions.assertThat(result!!.size).isEqualTo(1)
        val recipe: Map<String, Any> = result.get(0)
        Assertions.assertThat(recipe?.get("title")).isEqualTo("Tarte aux carottes et oignons")
    }

    @Test
    fun testSimpleSearchInstructionsMatch() {
        insertTarteCarottesOignons()
        val keywords = "chaud"
        val result: List<Map<String, Any>>? = this.restTemplate?.getForObject("http://localhost:$port/search?keywords=$keywords")
        Assertions.assertThat(result).isNotNull
        Assertions.assertThat(result!!.size).isEqualTo(1)
        val recipe: Map<String, Any> = result.get(0)
        Assertions.assertThat(recipe?.get("title")).isEqualTo("Tarte aux carottes et oignons")
    }

    @Test
    fun testSimpleSearchNoOtherMatch() {
        insertTarteCarottesOignons()
        val keywords = "nomatch"
        val result: List<Map<String, Any>>? = this.restTemplate?.getForObject("http://localhost:$port/search?keywords=$keywords")
        Assertions.assertThat(result).isNotNull
        Assertions.assertThat(result!!.size).isEqualTo(0)
    }

    private fun insertTarteCarottesOignons() {
        elasticQueryUtil!!.insertObject("5b9c12c96afd9d3c518006de",
                mapOf(
                        "title" to "Tarte aux carottes et oignons",
                        "ingredients" to listOf(
                                mapOf("name" to "pâte brisée", "quantity" to 250, "unit" to "g"),
                                mapOf("name" to "carotte", "quantity" to 400, "unit" to "g"),
                                mapOf("name" to "oignon", "quantity" to 3, "unit" to ""),
                                mapOf("name" to "crème fraîche", "quantity" to 25, "unit" to "cl"),
                                mapOf("name" to "oeuf", "quantity" to 1, "unit" to ""),
                                mapOf("name" to "carvi", "quantity" to 1, "unit" to "cuillère à café"),
                                mapOf("name" to "sel", "quantity" to 0, "unit" to ""),
                                mapOf("name" to "poivre", "quantity" to 0, "unit" to ""),
                                mapOf("name" to "pâte brisée", "quantity" to 250, "unit" to "g")
                        ),
                        "instructions" to "Peler les oignons, les couper en petits dés, et les faire fondre dans un peu d'huile. Râper les carottes. Battre la crème avec le carvi, et saler. Rajouter l’œuf, et mélanger avec les carottes. Taler la tarte dans un moule, et la piquer avec une fourchette. Verser les oignons cuits sur la pâte, puis le mélange carottes - crème, et enfourner à th 6 (180°C), pendant environ 20 min. Servir chaud ou froid.",
                        "url" to "nomatch",
                        "updateDate" to Date().time
                )
        )
    }
}