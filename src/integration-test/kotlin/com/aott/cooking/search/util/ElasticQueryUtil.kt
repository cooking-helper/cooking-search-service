package com.aott.cooking.search.util

import com.aott.cooking.cookingsearchservice.searchengine.elastic.ElasticConfig
import org.apache.http.HttpHost
import org.elasticsearch.action.delete.DeleteRequest
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient


class ElasticQueryUtil(val config: ElasticConfig) {
    val INDEX = "cooking"
    val TYPE = "recipe"

    val client: RestHighLevelClient = createClient()

    private fun createClient(): RestHighLevelClient {
        try {
            return RestHighLevelClient(
                    RestClient.builder(
                            HttpHost(config.getServer(), Integer.parseInt(config.getPort()), "http")).build())

        } catch (e: Exception) {
            throw Exception(e)
        }
    }

    fun updateObject(sourceObjectId: String, elasticObject: Map<String, Any>) {
        insertOrUpdate(sourceObjectId, elasticObject)
    }

    fun insertObject(sourceObjectId: String, elasticObject: Map<String, Any>) {
        insertOrUpdate(sourceObjectId, elasticObject)
    }

    private fun insertOrUpdate(sourceObjectId: String, elasticObject: Map<String, Any>) {
        val insertRequest = IndexRequest(INDEX, TYPE, sourceObjectId)
                .source(elasticObject)
        client.index(insertRequest)
    }

    fun deleteObject(objectIdToDelete: String) {
        val request = DeleteRequest(
                INDEX,
                TYPE,
                objectIdToDelete)


        client.delete(request)
    }
}
